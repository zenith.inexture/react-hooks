import React, { useEffect, useState } from "react";

function UseEffect() {
  const [count, setCount] = useState(0);
  const [name, setName] = useState("");
  const [x, setX] = useState(0);
  const [y, setY] = useState(0);

  useEffect(() => {
    console.log("runs every time", count);
  });

  useEffect(() => {
    console.log("runs only on dependency changes", count);
  }, [count]);

  const setMousePostion = (e) => {
    setX(e.clientX);
    setY(e.clientY);
  };

  useEffect(() => {
    console.log("runs only one time on componet mount");
    window.addEventListener("mousemove", setMousePostion);

    //this is the code for unmouting the eventlistner form the code
    //if we don't remove this then it's does memory leak and might add some issues in the code
    return () => {
      console.log("component unmounting code");
      window.removeEventListener("mousemove", setMousePostion);
    };
  }, []);

  return (
    <>
      <h4>useEffect hook</h4>
      <input
        value={name}
        onChange={(e) => setName(e.target.value)}
        type="text"
      ></input>
      <button onClick={() => setCount(count + 1)}>Add one value</button>
      <h3>
        x - {x} : y - {y}
      </h3>
    </>
  );
}

export default UseEffect;
