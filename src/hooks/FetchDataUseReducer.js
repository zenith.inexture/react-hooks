import axios from "axios";
import React, { useEffect, useReducer } from "react";

//initialState for the data fetching request
const initialState = {
  loading: true,
  error: "",
  data: {},
};

//reducer for store the data in state
const reducer = (state, action) => {
  switch (action.type) {
    case "SUCCESSFULL":
      return {
        ...state,
        loading: false,
        data: action.payload,
      };
    case "FAIL":
      return {
        ...state,
        loading: false,
        error: "error occoured while fetching the data",
      };
    default:
      return state;
  }
};

function FetchDataUseReducer() {
  //useReducer hook provide the state and dispatch method for fetching the data
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/users/1")
      .then((res) => {
        dispatch({
          type: "SUCCESSFULL",
          payload: res.data,
        });
      })
      .catch(() => {
        dispatch({
          type: "FAIL",
        });
      });
  }, []);

  return (
    <>
      <h4>Data Fetching with useReducer hook</h4>
      {state.loading ? "Loading...." : <p>{state.data.name}</p>}
      {state.error && <p>{state.error}</p>}
    </>
  );
}

export default FetchDataUseReducer;
