import React, { useCallback, useState } from "react";
import Callbackbutton from "../components/Callbackbutton";
import CallbackDispaly from "../components/CallbackDisplay";
import Title from "../components/Title";

function UseCallback() {
  const [age, setAge] = useState(0);
  const [salary, setSalary] = useState(0);

  //two function for increment both the value
  const addAge = useCallback(() => {
    setAge((preage) => preage + 1);
  }, [age]);

  const addSalary = useCallback(() => {
    setSalary((presalary) => presalary + 10000);
  }, [salary]);

  //without useCallback function the one of the component is re-rendered unnecessary while other component is changing
  // const addAge = () => {
  //   setAge((preage) => preage + 1);
  // };

  // const addSalary = () => {
  //   setSalary((presalary) => presalary + 10000);
  // };

  //return more-then one compoenent for the understanding to re-render child component
  return (
    <>
      <Title />
      <CallbackDispaly text="Age" count={age} />
      <Callbackbutton handleClick={addAge}>Add Age</Callbackbutton>
      <CallbackDispaly text="Salary" count={salary} />
      <Callbackbutton handleClick={addSalary}>Add Salary</Callbackbutton>
    </>
  );
}

export default UseCallback;

//we can see that without any use of hooks and feature the all child component re-render evey time when state is changed
//if we want to avoid this re-render then we have to use react-memo or useMemo or useCallback hook

//we used React.memo for avoid the re-render
//but the handleClick props of Callbackbutton component is re-render every time because every time the changes the compoenent re-render, so this addSalary function created every time when u change any state

//avoid this effect in child component function then we have to use useCallback hook
