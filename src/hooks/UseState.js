import React, { useState } from "react";

function UseState() {
  //create a state with useState hook in function component
  //it's return variable and another is setfunction for that variable
  const [count, setCount] = useState(0);
  //bydefault initialize the value with zeor

  //state with object value
  const [info, setInfo] = useState({ firstname: "", lastname: "poriya" });

  //addCount for adding the count value
  const addCount = () => {
    // setCount(count + 1);

    //if we are calling this addcout function five time in a row then we have to use previous state without that it will increment only one time
    setCount((precount) => precount + 1);
  };

  //function for calling add count five time in a row
  const addFiveCount = () => {
    addCount();
    addCount();
    addCount();
    addCount();
    addCount();
  };

  //function for chaning the firstname in object state
  const changeInfoFun = () => {
    //we have to use sprade operator in object when we modify any one value in object
    //this is happend same in Array object
    setInfo({ ...info, firstname: "zenith" });
  };
  return (
    <>
      <h3>useState hook</h3>
      <h4>{count}</h4>
      <button onClick={() => addCount()}>Add 1</button>
      <button onClick={() => addFiveCount()}>Add 5</button>
      <h4>
        {info.firstname} {info.lastname}
      </h4>
      <button onClick={() => changeInfoFun()}>Add the firstname</button>
    </>
  );
}

export default UseState;
