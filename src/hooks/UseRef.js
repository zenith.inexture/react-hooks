import React, { useEffect, useRef, useState } from "react";

function UseRef() {
  const [timer, setTimer] = useState(0);
  const timerRef = useRef();
  const inputRef = useRef();

  //we can access the ref any place in functional component
  useEffect(() => {
    //start timer with the useRef hook
    //if we use the class component then we can easily remove the interval
    //if we write our code like this [ const interval = ] then we can't access this variable inside the return

    timerRef.current = setInterval(() => {
      setTimer((pretimer) => pretimer + 1);
    }, 1000);

    //unmount the interval
    return () => {
      clearInterval(timerRef.current);
    };
  }, []);

  //we can access this ref on function call also
  const handleFocus = () => {
    inputRef.current.focus();
  };

  return (
    <>
      <h4>useRef hook</h4>
      <input type="text" ref={inputRef}></input>
      <button onClick={handleFocus}>Focus this input field</button>

      <p>Timer : {timer}</p>
      <button onClick={() => clearInterval(timerRef.current)}>
        Clear hook Timer
      </button>
    </>
  );
}

export default UseRef;
