import { useEffect } from "react";

function useTitle(count) {
  useEffect(() => {
    document.title = `hello, ${count} times`;
  }, [count]);
}

export default useTitle;
