import React from "react";

function Callbackbutton({ handleClick, children }) {
  console.log(`${children} button rendered`);
  return <button onClick={handleClick}>{children}</button>;
}

export default React.memo(Callbackbutton);
