//usereducer hook used to manages state
//usereducer - reducer like array reduce method

import { useReducer } from "react";

//initialState for the fucntion
const initialState = 0;

//reducer function for the useReducer hook
//this reducer is same as the reducer method in redux
const reducer = (state, action) => {
  switch (action) {
    case "increment":
      return state + 1;
    case "decrement":
      return state - 1;
    case "reset":
      return initialState;
    default:
      return state;
  }
};

function Usereducer() {
  //useRedecer function returns the two value in return one is newstate and another is dispatch function
  //useReducer(reducer(function), initialstate)
  const [count, dispatch] = useReducer(reducer, initialState);
  return (
    <>
      <h4>useReducer hook</h4>
      <div>Count : {count}</div>
      <button onClick={() => dispatch("increment")}>Increment</button>
      <button onClick={() => dispatch("decrement")}>Decrement</button>
      <button onClick={() => dispatch("reset")}>Reset</button>
    </>
  );
}
export default Usereducer;
