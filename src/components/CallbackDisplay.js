import React from "react";

function CallbackDispaly({ text, count }) {
  console.log(`${text} display component rendered`);
  return (
    <div>
      {text} : {count}
    </div>
  );
}

export default React.memo(CallbackDispaly);
