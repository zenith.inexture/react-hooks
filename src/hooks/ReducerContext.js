import React, { useContext } from "react";
import { CountContext } from "../App";

function ReducerContext() {
  const countContext = useContext(CountContext);
  return (
    <>
      <h4>useReducer + useCotext</h4>
      <div>Count : {countContext.countValue}</div>
      <button onClick={() => countContext.countDispatch({ type: "increment" })}>
        Increment
      </button>
      <button onClick={() => countContext.countDispatch({ type: "decrement" })}>
        Decrement
      </button>
      <button onClick={() => countContext.countDispatch({ type: "reset" })}>
        Reset
      </button>
    </>
  );
}

export default ReducerContext;
