import React from "react";
import BReducerContext from "./BReducerContext";

function AReducerContext() {
  return <BReducerContext />;
}

export default AReducerContext;
