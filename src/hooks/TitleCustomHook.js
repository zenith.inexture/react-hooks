import React, { useState } from "react";
import useTitle from "../customhook/useTitle";

function TitleCustomHook() {
  const [count, setCount] = useState(0);

  //this is custom hooks for changing the value of document titles
  useTitle(count);

  return (
    <>
      <h4>Title Custom hook</h4>
      <p>{count}</p>
      <button onClick={() => setCount((precount) => precount + 1)}>
        Increase count value
      </button>
    </>
  );
}

export default TitleCustomHook;
