import React from "react";

function Title() {
  console.log("title component rendered");
  return <h4>useCallback hook</h4>;
}

export default React.memo(Title);
