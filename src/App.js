import { createContext, useReducer } from "react";
import A from "./A";
import "./App.css";
import AReducerContext from "./AReducerContext";
import FetchDataUseEffect from "./hooks/FetchDataUseEffect";
import FetchDataUseReducer from "./hooks/FetchDataUseReducer";
import TitleCustomHook from "./hooks/TitleCustomHook";
import UseCallback from "./hooks/UseCallback";
import UseEffect from "./hooks/UseEffect";
import UseMemo from "./hooks/UseMemo";
import Usereducer from "./hooks/UseReducer";
import UseRef from "./hooks/UseRef";
import UseState from "./hooks/UseState";

//createContext for provide the value
export const UserOneContext = createContext();
export const UserTwoContext = createContext();

//for provide the value of reducer count in context
export const CountContext = createContext();
//initialState for the fucntion
const initialState = 0;

//reducer function for the useReducer hook
//this reducer is same as the reducer method in redux
const reducer = (state, action) => {
  switch (action.type) {
    case "increment":
      return state + 1;
    case "decrement":
      return state - 1;
    case "reset":
      return initialState;
    default:
      return state;
  }
};

function App() {
  //create reducer for doing the operation
  const [count, dispatch] = useReducer(reducer, initialState);

  return (
    <>
      <UseState />

      <UseEffect />

      {/* <FetchDataUseEffect /> */}

      {/* <UserOneContext.Provider value={"hii i'm user one"}>
        <UserTwoContext.Provider value={"hii i'm user two"}>
          <A />
        </UserTwoContext.Provider>
      </UserOneContext.Provider> */}

      {/* <Usereducer /> */}

      {/* <CountContext.Provider
        value={{ countValue: count, countDispatch: dispatch }}
      >
        <AReducerContext />
      </CountContext.Provider> */}

      {/* <FetchDataUseReducer /> */}

      {/* <UseCallback /> */}

      {/* <UseMemo /> */}

      {/* <UseRef /> */}

      {/* <TitleCustomHook /> */}
    </>
  );
}

export default App;
