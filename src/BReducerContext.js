import React from "react";
import ReducerContext from "./hooks/ReducerContext";

function BReducerContext() {
  return <ReducerContext />;
}

export default BReducerContext;
