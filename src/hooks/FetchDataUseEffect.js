import React, { useEffect, useState } from "react";
import axios from "axios";

function FetchDataUseEffect() {
  const [data, setData] = useState([]);
  const [inputfield, setInputField] = useState(1);
  const [finalinputfield, setFianlInputField] = useState(1);
  const [singledata, setSingleData] = useState({});

  //with empty dependency array we can fetch data form api in react application
  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then((res) => setData(res.data))
      .catch((err) => console.log(err));
  }, []);

  //fetch single data on the button click
  useEffect(() => {
    axios
      .get(`https://jsonplaceholder.typicode.com/users/${inputfield}`)
      .then((res) => setSingleData(res.data))
      .catch((err) => console.log(err));
  }, [finalinputfield]);

  const handleFinalChage = () => {
    setFianlInputField(inputfield);
  };
  return (
    <>
      <h2>All data</h2>
      {data.map((i) => (
        <p key={i.id}>{i.name}</p>
      ))}
      <h2>Fetch single data</h2>
      <input
        value={inputfield}
        onChange={(e) => setInputField(e.target.value)}
        type="number"
      ></input>
      <button onClick={() => handleFinalChage()}>Fetch</button>
      <p>{singledata.name}</p>
    </>
  );
}

export default FetchDataUseEffect;
