import React, { useState, useMemo } from "react";

//i have one value that calculate the value of the someting, and after this another state that are changes on it's dependency so, in this case the value of first component unnecessary calculate while other component is changes
//so, to avoid this situation we have to use the useMemo on the value
//so, the useMemo prevent the unnecessary calculation of the value , if the dependency changes then and then the component going to re-calculate the value

function Usememo() {
  const [number, setNumber] = useState(0);
  const [counter, setCounter] = useState(0);

  //if the number dependency will changes then and then the squredNum function runs and re-calculate the value other wise the value remains the same
  const squaredNum = useMemo(() => {
    return squareNum(number);
  }, [number]);

  //function without use of useMemo render every on the state changes in the component
  // const squaredNum = () => {
  //   return squareNum(number);
  // };

  //function for increment the count value
  const counterHander = () => {
    setCounter(counter + 1);
    console.log("counter updated successfully");
  };

  return (
    <>
      <h4>useMemo hook</h4>
      <input
        type="number"
        placeholder="Enter a number"
        value={number}
        onChange={(e) => setNumber(e.target.value)}
      ></input>
      <div>OUTPUT: {squaredNum}</div>
      {/*both have different state so the re-render is happends, to avoid that we used the useMemo hook  */}

      <button onClick={counterHander}>Add</button>
      <div>Counter : {counter}</div>
    </>
  );
}

// function to square the value
function squareNum(number) {
  console.log("Squaring will be done!");
  return Math.pow(number, 2);
}

export default Usememo;
