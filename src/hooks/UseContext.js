import React, { useContext } from "react";
import { UserOneContext, UserTwoContext } from "../App";

function UseContext() {
  //with the help of useContext hook we can write simpler code for consume the context value
  const valueContextOne = useContext(UserOneContext);
  const valueContextTwo = useContext(UserTwoContext);

  //now we can directly use this context value useContext hook fetch the context value for us
  return (
    <div>
      {valueContextOne} {valueContextTwo}
    </div>
  );
}

export default UseContext;
