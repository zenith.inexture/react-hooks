import React, { useContext } from "react";
import UseContext from "./hooks/UseContext";

function B() {
  return <UseContext />;
}

export default B;
